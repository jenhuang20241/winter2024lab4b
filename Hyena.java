import java.util.Scanner;
import java.util.Random;

public class Hyena {
	Random rand = new Random();
	
	private double size;
	private int agility;
	private int wit;
	
	
	//constructor
	public Hyena(double size, int agility, int wit){
		this.size = size;
		this.agility = agility;
		this.wit = wit;
	}
	
	
	//setter method
	public void setSize(double newSize){
		this.size = newSize;
	}

	
	//getter methods
	public double getSize(){
		return this.size;
	}
	
	public int getAgility(){
		return this.agility;
	}
	
	public int getWit(){
		return this.wit;
	}
	
	
	
	public void search(){
		int result = rand.nextInt(20);
		
		System.out.println("Searching...");
		System.out.println("You rolled a " + result + " + " + wit + "(wit)");
		result += wit;
		
		System.out.println("Result: " + result);
		
		if (result >= 10) {
			System.out.println("Success!!");
		}
		else {
			System.out.println("Nothing found...");
			
		}
		
	}
	
	public void maul(){
		System.out.println("Your hyena chomps down.");
	}
}