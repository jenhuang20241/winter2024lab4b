import java.util.Scanner;

public class VirtualPetApp {
	public static void main(String[] args) { 
		java.util.Scanner reader = new java.util.Scanner(System.in);
		Hyena[] cackle = new Hyena[1];
		
		for (int i = 0; i < cackle.length; i++) {
			
			System.out.println("What is Hyena " + (i + 1) + "'s Size? (Double)");
			double newSize = Double.parseDouble(reader.nextLine());
			
			System.out.println("What is Hyena " + (i + 1) + "'s Agility? (Int)");
			int newAgility = Integer.parseInt(reader.nextLine());
			
			System.out.println("What is Hyena " + (i + 1) + "'s Wit? (Int)");
			int newWit = Integer.parseInt(reader.nextLine());
			
			cackle[i] = new Hyena(newSize, newAgility, newWit);
		}
		
		//setting the size of the last Hyena
		System.out.println("What is Hyena " + (cackle.length + 1) + "'s NEW Size? (Double)");
		double newSize = Double.parseDouble(reader.nextLine());
		cackle[cackle.length-1].setSize(newSize);
		
		//printing all fields of last Hyena
		System.out.println("New Size: " + cackle[cackle.length-1].getSize());
		System.out.println(cackle[cackle.length-1].getAgility());
		System.out.println(cackle[cackle.length-1].getWit());
		
		
	}
}